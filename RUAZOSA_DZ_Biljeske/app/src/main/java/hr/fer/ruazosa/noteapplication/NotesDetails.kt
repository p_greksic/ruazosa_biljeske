package hr.fer.ruazosa.noteapplication

import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_notes_details.*
import java.util.*

class NotesDetails : AppCompatActivity() {
    private val NOTES_DETAILS_ACTIVITY_REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_details)
        val extras = intent.extras
        if(extras != null){
            // provjeri request code
            if(extras.getInt("Request_Code") == NOTES_DETAILS_ACTIVITY_REQUEST_CODE){
                noteTitleEditText.text = Editable.Factory.getInstance().newEditable(extras.getString("Note_Title"))
                noteDescriptionEditText.text = Editable.Factory.getInstance().newEditable(extras.getString("Note_Description"))
                saveNoteButton.text = getString(R.string.update_note_button_string)

                saveNoteButton.setOnClickListener {
                    // postavi povratne vrijednosti za MainActivity
                    val intent = Intent()
                    intent.putExtra("Note_Title", noteTitleEditText.text.toString())
                    intent.putExtra("Note_Description", noteDescriptionEditText.text.toString())
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }



        }else {
            val viewModel =
                ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                    NotesViewModel::class.java
                )

            saveNoteButton.setOnClickListener {
                var note = Note()
                note.noteTitle = noteTitleEditText.text.toString()
                note.noteDescription = noteDescriptionEditText.text.toString()
                note.noteDate = Date()
                viewModel.saveNoteToRepository(note)
                finish()
            }
        }
    }


}
