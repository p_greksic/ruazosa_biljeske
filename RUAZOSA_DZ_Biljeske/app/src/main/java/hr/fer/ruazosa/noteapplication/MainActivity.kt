package hr.fer.ruazosa.noteapplication

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), OnItemClickListener {

    private val NOTES_DETAILS_ACTIVITY_REQUEST_CODE = 1
    private var oldNote: Note? = null;
    lateinit var notesAdapter: NotesAdapter
    lateinit var viewModel: NotesViewModel

    override fun onItemClicked(note: Note) {
        //Toast.makeText(this,"Note was selected!\n Note title: ${note.noteTitle} \n", Toast.LENGTH_LONG).show()
        oldNote = note
        val intent = Intent(this, NotesDetails::class.java)
        // ovdje moze predati i object Note ali tada Note treba biti Parceable
        // tj. implementirati to sucelje
        intent.putExtra("Request_Code", NOTES_DETAILS_ACTIVITY_REQUEST_CODE)
        intent.putExtra("Note_Title", note.noteTitle)
        intent.putExtra("Note_Description", note.noteDescription)
        startActivityForResult(intent, NOTES_DETAILS_ACTIVITY_REQUEST_CODE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listOfNotesView.layoutManager = LinearLayoutManager(applicationContext)

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.cell_divider)!!)
        listOfNotesView.addItemDecoration(decorator)

        viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)

        notesAdapter = NotesAdapter(viewModel, this)
        listOfNotesView.adapter = notesAdapter

        viewModel.notesList.observe(this, Observer {
            notesAdapter.notifyDataSetChanged()
        })

        newNoteActionButton.setOnClickListener {
            val intent = Intent(this, NotesDetails::class.java)
            startActivity(intent)
        }
    }


    override fun onResume() {
        super.onResume()
        viewModel.getNotesRepository()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Check that it is the NotesDetails activity with an OK result
        if (requestCode == NOTES_DETAILS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if(data != null){
                    // dohvati podatke o novoj biljesci
                    val newNoteTitle = data!!.getStringExtra("Note_Title")
                    val newNoteDescription = data!!.getStringExtra("Note_Description")
                    var note: Note = Note()
                    note.noteTitle = newNoteTitle
                    note.noteDescription = newNoteDescription
                    note.noteDate = Date()
                    // azuriraj biljesku
                    if((oldNote!!.noteTitle != note.noteTitle) || (oldNote!!.noteDescription != note.noteDescription)){
                        if(oldNote != null) {
                            viewModel.updateNoteFromRepository(oldNote!!, note)
                            viewModel.getNotesRepository()
                        }
                    }
                }
            }
        }
    }

}