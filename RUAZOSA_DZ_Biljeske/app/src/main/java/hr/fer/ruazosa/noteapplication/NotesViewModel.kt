package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class NotesViewModel: ViewModel() {
    var notesList = MutableLiveData<List<Note>>()

    fun getNotesRepository() {
        notesList.value = NotesRepository.notesList
    }

    fun saveNoteToRepository(note: Note) {
        NotesRepository.notesList.add(note)
    }

    fun deleteNoteFromRepository(note: Note){
        NotesRepository.notesList.remove(note)
    }

    fun updateNoteFromRepository(oldNote: Note, newNote: Note){
        //dohvati index stare biljeske
        val noteIndex = NotesRepository.notesList.indexOf(oldNote)
        //ukloni staru biljesku
        deleteNoteFromRepository(oldNote)
        //umetni novu biljesku na mjestu stare
        NotesRepository.notesList.add(noteIndex, newNote)
    }
}